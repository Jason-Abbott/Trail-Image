var Enum = require('../enum.js');
var setting = require('../settings.js');

exports.view = function(req, res)
{
	res.redirect(Enum.httpStatus.permanentRedirect, 'http://issues.' + setting.domain);
};